/*
 *
 * 1.reset
 * 2.hover img
 * 3.footer
 * 4.other
 *
*/

/*
 1.reset [
----------------------------------------------------------- */
$(function(){
	//外部リンクにtarget="_blank"を付与
	$("a[href^='http://']").attr("target","_blank");
	$("a[href$='.pdf']").attr("target","_blank");
	// IE8以下にlast-child, first-child 追加
	if (!$.support.opacity) {
		$('body :first-child').addClass('firstChild');
		$('body :last-child').addClass('lastChild');
	}
	// $('ul, ol').each(function(){
	// 	$(this).children('li:odd').addClass('even');
	// 	$(this).children('li:even').addClass('odd');
	// });
	// $('table, tbody').each(function(){
	// 	$(this).children('tr:odd').addClass('even');
	// 	$(this).children('tr:even').addClass('odd');
	// });
});

/*
 2.hover img [
----------------------------------------------------------- */
$(function(){
	if ($.support.opacity) {
		$("a img").hover(
			function(){
				$(this).animate({opacity: 0.7},100);
			},
			function(){
				$(this).animate({opacity: 1},100);
			}
		);
	} else {
		$("a img:not([src$='.png'])").hover(
			function(){
				$(this).animate({opacity: 0.7},100);
			},
			function(){
				$(this).animate({opacity: 1},100);
			}
		);
	}
});

/*
 3.footer [
----------------------------------------------------------- */
/*$(function(){
	$("#main").css("min-height",($(window).height()-$("#header").height()-$("#footer").height()));
});
*/
/*
 4.other [
----------------------------------------------------------- */

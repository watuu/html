<?php
	///////////////////////////////////////////////////////////////////
	// ユーザー定義関数ライブラリ(smarty.php) ver 1.0
	///////////////////////////////////////////////////////////////////

	
	class MySmarty extends Smarty {
		
		function MySmarty($object_name="") {

			$this->template_dir = Smarty_Template;
			$this->compile_dir = Smarty_Compile;
			//$this->config_dir = Smarty_Config;
			//$this->cache_dir = Smarty_Cache;
			$this->caching = false;
			// 入出力処理
			$this->register_prefilter('convert_encoding_to_logic');
			$this->register_postfilter('convert_encoding_to_template');
			// デリミタ設定
			$this->left_delimiter = '{{';
			$this->right_delimiter = '}}';
			
			#$this->assign('','');
		}
	
	}
	  
	// =========================================================
	// Smarty → SHIFT_JIS 対応関数
	// =========================================================
	
	function convert_encoding_to_logic($template_source) {
	    if (function_exists("mb_convert_encoding")) {
	        //文字コードを変換する
	        return mb_convert_encoding($template_source, Smarty_ENC, APP_ENC);
	    }
	    return $template_source;
	}
	function convert_encoding_to_template($template_source) {
	    if (function_exists("mb_convert_encoding")) {
	        //文字コードを変換する
	        return mb_convert_encoding($template_source, OUT_ENC, Smarty_ENC);
	    }
	    return $template_source;
	}

?>

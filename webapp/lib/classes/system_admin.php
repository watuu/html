<?php
	///////////////////////////////////////////////////////////////////
	// ユーザー定義関数ライブラリ(system.php) ver 1.0
	///////////////////////////////////////////////////////////////////

	
	// magic_quotes_gpc対策
	if (get_magic_quotes_gpc()) {
    function strip_magic_slashes($arr)
    {
			if (is_array($arr)) {
				return array_map('strip_magic_slashes', $arr);
			} else {
				#$arr = strip_tags($arr);
				return stripslashes($arr);
			}
    }
	
    $_GET     = strip_magic_slashes($_GET);
    $_POST    = strip_magic_slashes($_POST);
    $_REQUEST = strip_magic_slashes($_REQUEST);
	}

	// セッションスタート
	//session_cache_limiter("public");
	session_start();

?>

<?php
	///////////////////////////////////////////////////////////////////
	// ユーザー定義関数ライブラリ(system.php) ver 1.0
	// 最終更新日：2009/06/27
	///////////////////////////////////////////////////////////////////

	// magic_quotes_gpc対策
	if (get_magic_quotes_gpc()) {
    function strip_magic_slashes($arr)
    {
			if (is_array($arr)) {
				return array_map('strip_magic_slashes', $arr);
			} else {
				return stripslashes($arr);
			}
    }
    $_GET     = strip_magic_slashes($_GET);
    $_POST    = strip_magic_slashes($_POST);
    $_REQUEST = strip_magic_slashes($_REQUEST);
	}
    function sanitize($arr)
    {
			if (is_array($arr)) {
				return array_map('sanitize', $arr);
			} else {
				$arr = htmlspecialchars($arr);
				return han_kana($arr);
			}
    }
    $_GET     = sanitize($_GET);
    $_POST    = sanitize($_POST);
    $_REQUEST = sanitize($_REQUEST);

	// セッションスタート
	session_cache_limiter('public');
/*
	nocache	クライアントもプロキシもキャッシュ無効
	private_no_expire	クライアントのみキャッシュ有効 ( Exprieヘッダをクライアントへ送信しない)
	public	クライアントもプロキシもキャッシュ有効
*/
	session_start();

	function han_kana($text ) {
		if (function_exists("mb_convert_kana")){
			if (preg_match("/(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F])/", $text)) {
				$text = mb_convert_kana($text ,"KV", APP_ENC);
				return $text;
			} else { 
				return $text;
			}
		} else {
			return $text;
		}
	}
	
/*	
	mb_convert_kana($str, "r");
		r	 「全角」英字を「半角」に変換します。
		R	「半角」英字を「全角」に変換します。
		n	「全角」数字を「半角」に変換します。
		N	「半角」数字を「全角」に変換します。
		a	「全角」英数字を「半角」に変換します。
		A	「半角」英数字を「全角」に変換します （"a", "A" オプションに含まれる文字は、U+0022, U+0027, U+005C, U+007Eを除く U+0021 - U+007E の範囲です）。
		s	「全角」スペースを「半角」に変換します（U+3000 -> U+0020）。
		S	「半角」スペースを「全角」に変換します（U+0020 -> U+3000）。
		k	「全角カタカナ」を「半角カタカナ」に変換します。
		K	「半角カタカナ」を「全角カタカナ」に変換します。
		h	「全角ひらがな」を「半角カタカナ」に変換します。
		H	「半角カタカナ」を「全角ひらがな」に変換します。
		c	「全角カタカナ」を「全角ひらがな」に変換します。
		C	「全角ひらがな」を「全角カタカナ」に変換します。
		V	濁点付きの文字を一文字に変換します。"K", "H" と共に使用します。
*/
?>

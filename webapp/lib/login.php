<?php

	///////////////////////////////////////////////////////////////////
	// ログインユーザー側 共通読み込みファイル ver 1.0
	///////////////////////////////////////////////////////////////////
	// @auther KOUS
	///////////////////////////////////////////////////////////////////

	// Smartyクラスの呼び出し
	require_once LIB_DIR.'Smarty/Smarty.class.php';
	
	// ユーザー定義関数の呼び出し
	require_once LIB_DIR.'lib/classes/mysql.php';
	require_once LIB_DIR.'lib/classes/smarty.php';
	require_once LIB_DIR.'lib/classes/system.php';

	// ユーザーログインチェック
	if (!$_SESSION['auth']) {
		header("Location: ".HOME_URL);
		exit;
	}

	// Smartyオブジェクト
	
?>
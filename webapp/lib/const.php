<?php
##############################################################
# システム設定ファイル
# auther KOUS
##############################################################

// ■ 全体設定項目 -------------------------------------------

define('DS', DIRECTORY_SEPARATOR);

/*
// アプリケーショントップディレクトリ(*)
*/
define('URL', "http://192.168.24.58/");

/*
// アプリケーショントップディレクトリ(*)
*/
define('ROOT_DIR', dirname(dirname(dirname(__FILE__))).DS);

/*
// サイトトップページ(*)
*/
define('WEBROOT_DIR', ROOT_DIR);

/*
// ライブラリディレクトリ(*)
*/
define('APP_DIR', ROOT_DIR.'webapp'.DS);

/*
// アプリケーション文字エンコード(*)
*/
define( 'APP_ENC', 'utf-8');

/*
// output文字エンコード(*)
*/
define( 'OUT_ENC', 'utf-8');

/*
// Smarty文字エンコード(*)
*/
define( 'Smarty_ENC', 'EUC-JP');

/*
// Smartyテンプレート(*)
*/
define( 'Smarty_Template', ROOT_DIR.'template');
define( 'Smarty_Compile', ROOT_DIR.'template_c');

/*
// include パス
*/
ini_set('include_path', APP_DIR . 'data' . DS . PATH_SEPARATOR . ini_get('include_path'));


// ■ データベース設定項目 -----------------------------------

/*
// ホスト名(*)
*/
define( 'DB_HOST', 'localhost');

/*
// データベース名(*)
*/
define( 'DB_NAME', '');

/*
// データベースユーザ名(*)
*/
define( 'DB_USERNAME', '');

/*
// データベースパスワード(*)
*/
define( 'DB_PASSWORD', '');

/*
// データベース文字エンコード
*/
define( 'DB_ENC', 'EUC-JP');

// ■ メール設定 ---------------------------------------------

/*
// メールアドレス(*)
*/
define( 'ADM_MAIL', 'kojima@risefactorty.net');

/*
// システムエラー用メールアドレス
*/
define( 'ERR_MAIL', 'kojima@risefactorty.net');

/*
// システムエラー用メールタイトル
*/
define( 'ERR_MAIL_TITLE', '【システム障害】');

/*
// メール送信者
*/
define( 'ADM_MAILER', '');

/*
// メール文字エンコード
*/
define( 'MAIL_ENC', 'ISO-2022-JP');

// ■サイト設定項目 -----------------------------------------

/*
// セッションタイムアウト時間(分)
*/
define( 'SESSION_TIMEOUT', 30);

/*
// ページャー表示数
*/
define( 'LIST_NUM', 50);

/*
// 管理画面
*/
define('USERNAME', '');
define('PASSWORD', '');

// ■動作設定 ----------------------------------------------

/*
// デバッグ／リリースモードの設定(*)
//   開発中デバッグモードは1を指定。
//   完成リリースモードは0を指定。
//   ＜デバッグモード処理変更箇所＞
//   ・DBエラーの詳細表示
//   ・PHPエラー表示
*/
define ( 'DEBUG_MODE', 1);


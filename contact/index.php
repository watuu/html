<?php
// カレントディレクトリ
$basedir = '../';

// ライブラリインクルード
require_once ($basedir.'webapp/lib/const.php');
require_once ($basedir.'webapp/lib/public.php');

// 設定
$php_self = 'index.php'; //(*)
define('DEF_ACT', 'entry'); // 既定の画面(*)
define('MAIL_TO', 'ricky@risefactory.net'); // 送信先(*)
#define('MAIL_TO_CC', ADM_MAIL); // CC
$mail_copy = 1; // 送信者に控えを送る
$mail_txt = 'contact.txt'; // メール雛形(*)
$log_file = ''; // ログファイル(*)
$template_name = 'contact_'; // テンプレートの名前　例)contact_○○○(*)


$smarty = new MySmarty();

// =========================================================
// チェックボックス
// =========================================================
// チェックボックスはこちらで設定すると簡単です。
// HTML表示例
// {{html_checkboxes name='referer' options=$checkbox_1 separator='<br />'}}
/*
$checkbox_1 = array (
	'HP' => 'HP',
);
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	foreach($_POST as $k => $v) {
		if($k == 'catalog1'){
			foreach($v as $vv){
				foreach($checkbox_1 as $selected_k => $selected_v ) {
					if($vv == $selected_k){
						$checkbox_selected_1[] = $selected_k;
					}
				}
			}
		}
	}
}
$smarty->assign('checkbox_1', $checkbox_1);
$smarty->assign('checkbox_selected_1', $checkbox_selected_1);
*/

// =========================================================
// 入力データチェック
// =========================================================
// カスタムのエラーは入力データチェックの項目に直接記載してください。

$prm_check = array (
    'name' => 'お名前を入力してください',
    'name_kana' => 'ふりがなを入力してください',
    'address' => '住所を入力してください',
    'tel' => '電話番号を入力してください',
    'birth' => '生年月日を入力してください',
);
$check_mail = 'true';

// =========================================================
// Logic
// =========================================================
// フォームデータの変換

$param = '';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $param = $_POST;
    $error = chk_data($_POST,$prm_check);
    if (!empty($error) ) {
        $param['act'] = DEF_ACT;
        $smarty->assign('error', $error);
    }
}

// 処理ページの決定
$act = isset($param['act']) ? $param['act'] : DEF_ACT;
$screen_function_name = 'screen_' . $act;
if (!function_exists($screen_function_name)) {
    exit();
}
call_user_func($screen_function_name, $param);
$temp_name = $act . '.html';

// テンプレートを表示
$smarty->assign('url', $php_self);
$smarty->display($template_name.$temp_name);

// =========================================================
// 入力画面
// =========================================================

function screen_entry($param) {
    global $smarty;

    $smarty->assign('prm', $param);
    $_SESSION['entry'] = true;

    return TRUE;
}

// =========================================================
// 確認画面
// =========================================================

function screen_confirm($param) {
    global $smarty;

    // actの初期化
    unset($param['act']);

    $smarty->assign('prm', $param);
    $smarty->assign('hidden', $param);

    return TRUE;
}

// =========================================================
// 完了画面
// =========================================================

function screen_submit($param) {
    global $smarty;
    global $mail_txt;
    global $mail_copy;
    global $log_file;

    // ログ書き出し
    if($log_file != ""){
        $log_file = APP_DIR .'data'. DS . $log_file;

        $handle = @fopen($log_file, 'a+');
        if($handle){
            flock($handle, LOCK_EX);
            rewind($handle);
            foreach($param as $v) {
                if(is_array($v)) {
                    foreach($v as $vv) {
                        $put_data[] = str_replace(array("\r\n", "\n", "\r"),'<br />', $vv);
                    }
                }else{
                    $put_data[] = str_replace(array("\r\n", "\n", "\r"),'<br />', $v);
                }
            }
            $put_data = implode(',', $put_data . '\n');
            fputs($handle, $put_data);
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }

    extract($param);

    // メール送信
    require_once ($mail_txt);

    mb_language('Japanese');
    mb_internal_encoding( APP_ENC );
    //管理者へメール
    if(defined('MAIL_TO')){
        if(!mb_send_mail(MAIL_TO, $mailtitle, $temp , 'From:'. $mail)) {
            echo 'メール送信が失敗しました。';
            exit();
        }
    }
    //CCへメール
    if(defined('MAIL_TO_CC')){
        if(!mb_send_mail(MAIL_TO_CC, $mailtitle, $temp , 'From:'. $mail)) {
            echo 'メール送信が失敗しました。';
            exit();
        }
    }
    //ユーザー（$from）へメール
    if(!empty($mail) && $mail_copy == 1){
        if(!mb_send_mail($mail, $mailtitle, $temp , 'From:'. MAIL_TO)) {
            echo '控えのメール送信が失敗しました。';
            exit();
        }
    }

    // session_destroy();

    return TRUE;
}

// =========================================================
// 入力データチェック
// =========================================================

function chk_data($prm_data,$prm_check) {
    global $check_mail;

    $error = array();
    foreach ($prm_data as $k => $v) {
        foreach ($prm_check as $kk => $vv) {
            if($k == $kk){
                if(empty($v)) {
                    $error[$kk] = $vv;
                }
            }
        }
    }
    if($check_mail){
        if ($prm_data['mail'] == '') {
            $error['mail'] = 'メールアドレスを入力してください';
        }elseif (chk_mail($prm_data['mail']) === false){
            $error['mail'] = 'メールアドレスをご確認ください';
        }
    }
// カスタムのエラーは以下に記述してください。
//    if ($prm_data['comment'] == '') {
//        $error[] = 'お問合せ内容をご入力ください';
//    }
    return $error;
}

function chk_mail($mail, $domainCheck = false)
{
    if (preg_match('/^[a-zA-Z0-9\.\_\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+\@(\[?)[a-zA-Z0-9\-\.]+'.
        '\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/', $mail)) {
        if ($domainCheck && function_exists('checkdnsrr')) {
            list (, $domain)  = explode('@', $mail);
            if (checkdnsrr($domain, 'MX') || checkdnsrr($domain, 'A')) {
                return true;
            }
            return false;
        }
        return true;
    }
    return false;
}


?>